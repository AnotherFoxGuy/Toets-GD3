﻿using UnityEngine;

public class Magic : MonoBehaviour
{
    private MagicEntity.Type _magicType;

    public MagicEntity.Type MagicType
    {
        get { return _magicType; }
        set { _magicType = value; }
    }

    private GameObject _ice, _fire;

    private void Start()
    {
        _ice = Resources.Load<GameObject>("Ice");
        _fire = Resources.Load<GameObject>("Fire");
    }

    public void Cast(Vector3 to)
    {
        var pos = transform.position + to * 2;
        pos.y = transform.position.y;
        MagicEntity i;
        switch (MagicType)
        {
            case MagicEntity.Type.Fire:
                i = Instantiate(_fire, pos, Quaternion.identity).GetComponent<MagicEntity>();
                break;
            case MagicEntity.Type.Ice:
                i = Instantiate(_ice, pos, Quaternion.identity).GetComponent<MagicEntity>();
                break;
            default:
                i = Instantiate(_ice, pos, Quaternion.identity).GetComponent<MagicEntity>();
                break;
        }

        Destroy(i.gameObject, 5);
        i.Damage = 10;
        to.y = 0;
        i.FlyTo = to;
        i.MagicType = MagicType;
    }
}