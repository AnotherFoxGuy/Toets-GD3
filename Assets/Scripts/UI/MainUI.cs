using MarkLight.Views.UI;
using UnityEngine.SocialPlatforms.Impl;

public class MainUI : UIView
{
    public string Type;
    public int Score;
    private int _score;
    

    public void SetType(string s)
    {
        SetValue(() => Type, s); // Volume = 75.0f
    }
    public void AddScore()
    {
        _score++;
        SetValue(() => Score, _score); // Volume = 75.0f
    }
}