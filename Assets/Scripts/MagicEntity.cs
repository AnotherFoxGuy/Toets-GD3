﻿using System;
using UnityEngine;

public class MagicEntity : MonoBehaviour
{
    public enum Type
    {
        Fire,
        Ice
    }

    public Type MagicType;
    public float Damage;
    public Vector3 FlyTo;

    private void Update()
    {
        transform.Translate(FlyTo);
    }

    private void OnTriggerEnter(Collider other)
    {
        Enemy.DamageClass dmg;
        switch (MagicType)
        {
            case Type.Fire:
                dmg = Enemy.DamageClass.Fire;             
                break;
            case Type.Ice:
                dmg = Enemy.DamageClass.Ice;            
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        other.SendMessage("TakeDamage", dmg, SendMessageOptions.DontRequireReceiver);
        Destroy(gameObject);
    }
}