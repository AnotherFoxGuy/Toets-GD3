using UnityEngine;

public class Cam : MonoBehaviour
{
    public Transform CamTransform;

    // Use this for initialization
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        if (!V3Equal(CamTransform.position, transform.position))
        {
            var dif = CamTransform.position - transform.position;
            dif /= 10;
            CamTransform.position -= dif;
        }
    }

    public bool V3Equal(Vector3 a, Vector3 b)
    {
        return Vector3.SqrMagnitude(a - b) < 0.1;
    }
}