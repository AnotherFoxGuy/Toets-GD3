﻿using UnityEngine;

public class Zwaard : MonoBehaviour
{
    private GoTweenChain _tween;

    private void Start()
    {
        _tween = new GoTweenChain();
        _tween.append(new GoTween(transform, 0.1f, new GoTweenConfig().localRotation(Vector3.down * 10)))
            .append(new GoTween(transform, 0.1f, new GoTweenConfig().localRotation(Vector3.left * 90)));
    }

    public void Swing()
    {
        _tween.restart();
    }

    private void OnTriggerEnter(Collider other)
    {
        other.SendMessage("TakeDamage", Enemy.DamageClass.Sword, SendMessageOptions.DontRequireReceiver);
    }
}