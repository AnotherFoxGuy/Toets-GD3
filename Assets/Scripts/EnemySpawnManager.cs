﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnManager : MonoBehaviour
{
    private List<GameObject> _objects = new List<GameObject>();

    private float _range = 50;
    private int _score;

    public GameObject Door;

    // Use this for initialization
    void Start()
    {
        _objects.Add(Resources.Load<GameObject>("Skeleton"));
        _objects.Add(Resources.Load<GameObject>("Blob"));
        _objects.Add(Resources.Load<GameObject>("Dragon"));

        for (var i = 0; i < 5; i++)
        {
            var e = Instantiate(_objects[Random.Range(0, _objects.Count)], Random.insideUnitCircle * _range,
                Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.P))
            _score = 523;
    }

    public void Died(Enemy enemy)
    {
        _score++;
        Instantiate(_objects[Random.Range(0, _objects.Count)], Random.insideUnitCircle * _range, Quaternion.identity);
        if (_score > 100)
        {
            print("GOOOOOOOw");
            _score = 0;
            Instantiate(Resources.Load<GameObject>("Key"), Random.insideUnitCircle * _range, Quaternion.identity);

            var t = Go.to(Door.transform, 4f,
                new GoTweenConfig().shake(Vector3.one,GoShakeType.Scale).position(new Vector3(-50,-3.5f)));
            t.play();
        }
    }
}