﻿using UnityEngine;

public class Player : MonoBehaviour
{
    private CharacterController _characterController;
    private float _health = 100;
    private Zwaard _zwaard;
    private Magic _magic;
    private readonly float speed = 10;
    public GameObject Ui;
    private MainUI _ui;
    private RaycastHit _hit;

    // Use this for initialization
    private void Start()
    {
        _characterController = GetComponent<CharacterController>();
        _zwaard = GetComponentInChildren<Zwaard>();
        _magic = GetComponent<Magic>();
        _ui = Ui.GetComponentInChildren<MainUI>();
        _ui.SetType(_magic.MagicType.ToString());
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKey(KeyCode.W))
            _characterController.SimpleMove(Vector3.forward * speed);
        if (Input.GetKey(KeyCode.S))
            _characterController.SimpleMove(Vector3.back * speed);
        if (Input.GetKey(KeyCode.A))
            _characterController.SimpleMove(Vector3.left * speed);
        if (Input.GetKey(KeyCode.D))
            _characterController.SimpleMove(Vector3.right * speed);

        if (Input.GetMouseButtonDown(0))
            _zwaard.Swing();
        if (Input.GetMouseButtonDown(01))
            _magic.Cast((_hit.point - transform.position).normalized);

        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        
        if (Physics.Raycast(ray, out _hit, 1000))
        {
            Debug.DrawLine(ray.origin, _hit.point);
            transform.LookAt(_hit.point);
        }
        if (Input.mouseScrollDelta.y != 0)
        {
            switch (_magic.MagicType)
            {
                case MagicEntity.Type.Fire:
                    _magic.MagicType = MagicEntity.Type.Ice;
                    break;
                case MagicEntity.Type.Ice:
                    _magic.MagicType = MagicEntity.Type.Fire;
                    break;
                default:
                    _magic.MagicType = MagicEntity.Type.Ice;
                    break;
            }

            _ui.SetType(_magic.MagicType.ToString());
        }
    }

    public void TakeDamage(float dmg)
    {
        _health -= dmg;
        print(name + ": " + _health);
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        print(hit.gameObject.tag);
        if (hit.gameObject.CompareTag("Coin"))
        {
            Destroy(hit.gameObject);
            _ui.AddScore();
        }
    }
}