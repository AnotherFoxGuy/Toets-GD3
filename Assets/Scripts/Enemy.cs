﻿using System;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class Enemy : MonoBehaviour
{
    public enum Class
    {
        Skeleton,
        Blob,
        Dragon
    }

    public enum DamageClass
    {
        Fire,
        Ice,
        Sword
    }

    private NavMeshAgent _agent;

    private float _health = 5;
    private GameObject _player;
    private float _timer;
    private Zwaard _zwaard;
    public Class EnemyClass;
    private Magic _magic;
    private EnemySpawnManager _spawnManager;
    private GameObject _coin;

    public float Health
    {
        get { return _health; }
        set { _health = value; }
    }

    // Use this for initialization
    private void Start()
    {
        _agent = GetComponent<NavMeshAgent>();
        _player = GameObject.FindWithTag("Player");
        _coin = Resources.Load<GameObject>("Coin");
        _agent.SetDestination(_player.transform.position);
        _spawnManager = GameObject.Find("EnemySpawnManager").GetComponent<EnemySpawnManager>();


        switch (EnemyClass)
        {
            case Class.Skeleton:
                _zwaard = GetComponentInChildren<Zwaard>();
                break;
            default:
                _magic = GetComponent<Magic>();
                break;
        }
    }

    // Update is called once per frame
    private void Update()
    {
        _timer -= Time.deltaTime;
        if (_timer < 0)
        {
            _timer = Random.Range(1, 3);
            DoDamage(1);
        }
    }


    public void TakeDamage(DamageClass damageClass)
    {
        switch (EnemyClass)
        {
            case Class.Skeleton:
                switch (damageClass)
                {
                    case DamageClass.Ice:
                        _health -= 0.5f;
                        break;
                    case DamageClass.Sword:
                        _health -= 2;
                        break;
                    default:
                        _health -= 1;
                        break;
                }

                break;
            case Class.Blob:
                switch (damageClass)
                {
                    case DamageClass.Sword:
                        _health -= 0.5f;
                        break;
                    default:
                        _health -= 1;
                        break;
                }
                break;
            case Class.Dragon:
                switch (damageClass)
                {
                    case DamageClass.Fire:
                        _health -= 0.5f;
                        break;
                    case DamageClass.Ice:
                        _health -= 2;
                        break;
                    default:
                        _health -= 1;
                        break;
                }
                break;
        }


        print(name + ": " + _health);
        if (_health <= 0)
            Die();
    }

    public void DoDamage(float dmg)
    {
        switch (EnemyClass)
        {
            case Class.Skeleton:
                _zwaard.Swing();
                break;
            default:
                _magic.Cast(transform.position);
                break;
        }
    }

    private void Die()
    {
        _spawnManager.Died(this);
        Instantiate(_coin, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}